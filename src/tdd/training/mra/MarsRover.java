package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private int[][] planet;
	private StringBuilder roverStatus;
	private static final String ROVER_LANDING_POINT = "(0,0,N)" ;
	private static final String CARDINAL_DIRECTIONS = "NESW";
	private static final int ROVER_FACING_INDEX = 5;
	private static final int ROVER_Y_INDEX = 3;
	private static final int ROVER_X_INDEX = 1;
	private StringBuilder commandSequence;
	private int currentCommandExec = 0;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		// To be implemented
		planet = new int[planetX][planetY];
		putObstaclesOnPlanet( planetObstacles );
		roverStatus = new StringBuilder();
		roverStatus.append(ROVER_LANDING_POINT);
	}
	
	
	

	private void putObstaclesOnPlanet(List<String> planetObstacles) {
		
		int numberOfObstacles = planetObstacles.size();
		
		String obstacle;
		
		for (int i = 0; i < numberOfObstacles; i++) {
			
			obstacle = planetObstacles.get(i);
			
			planet[obstacle.charAt(1) - '0'][obstacle.charAt(3) - '0'] = 1;
			
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return planet[x][y] == 1;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString.isEmpty()) {
			return ROVER_LANDING_POINT;
		}else{
			if( commandString == "r" || commandString == "l") {
				roverFacingDirectionUpdate(commandString);
				return roverStatus.toString();
			}else if( commandString == "f" || commandString == "b") {
				
				roverMovingUpdate(commandString);
				return roverStatus.toString();
				
			}else if( commandString.length() > 1 ) {
				
				commandSequence = new StringBuilder();
				commandSequence.append(commandString);
				multipleCommands();
				
			}
			return "errore";
		}
	}

	private void multipleCommands() throws MarsRoverException {
		
		for (int i = 0; i < commandSequence.length(); i++) {
			
			System.out.println(Character.toString(commandSequence.charAt(i)));
			System.out.println(executeCommand(Character.toString(commandSequence.charAt(i))));
			
		}
		
	}




	private char getRoverFacingDirection(){
		return roverStatus.charAt(ROVER_FACING_INDEX);
	}


	private void roverFacingDirectionUpdate(String commandString) {
		
		int currentDirectionIndex = CARDINAL_DIRECTIONS.indexOf(getRoverFacingDirection());
		
		if(commandString == "r" &&  getRoverFacingDirection() == 'W') {
			roverStatus.setCharAt(ROVER_FACING_INDEX, CARDINAL_DIRECTIONS.charAt(0));
		}else if(commandString == "l" &&  getRoverFacingDirection() == 'N') {
			roverStatus.setCharAt(ROVER_FACING_INDEX, CARDINAL_DIRECTIONS.charAt(3));
		}else {
			if(commandString == "r") {
				roverStatus.setCharAt(ROVER_FACING_INDEX, CARDINAL_DIRECTIONS.charAt(currentDirectionIndex + 1)); 
			}else if(commandString == "l") {
				roverStatus.setCharAt(ROVER_FACING_INDEX, CARDINAL_DIRECTIONS.charAt(currentDirectionIndex - 1)); 
			}
		}
	}
	
	private void roverMovingUpdate(String commandString) {
		
		if(commandString == "f" && roverStatus.charAt(ROVER_FACING_INDEX) == 'N') {
			System.out.println("dio");
			roverStatus.setCharAt(ROVER_Y_INDEX, getRoverIncreasedY()); 
		}else if(commandString == "b" && roverStatus.charAt(ROVER_FACING_INDEX) == 'N') {
			roverStatus.setCharAt(ROVER_Y_INDEX, getRoverDecreasedY()); 
		}else if(commandString == "f" && (roverStatus.charAt(ROVER_FACING_INDEX) == 'E')) {
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverIncreasedX()); 
		}else if(commandString == "b" && (roverStatus.charAt(ROVER_FACING_INDEX) == 'E')){
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverDecreasedX()); 
		}else if(commandString == "b" && roverStatus.charAt(ROVER_FACING_INDEX) == 'W') {
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverIncreasedX());
		}else if(commandString == "f" && roverStatus.charAt(ROVER_FACING_INDEX) == 'W') {
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverDecreasedX());
		}else if(commandString == "b" && roverStatus.charAt(ROVER_FACING_INDEX) == 'S') {
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverIncreasedY());
		}else if(commandString == "f" && roverStatus.charAt(ROVER_FACING_INDEX) == 'S') {
			roverStatus.setCharAt(ROVER_X_INDEX, getRoverDecreasedY());
		}
	}


/*	else if(commandString.length() > 1) {
		commandSequence = new StringBuilder();
		commandSequence.append(commandString);
		for (int i = 0; i < commandSequence.length(); i++) {
			commandString = Character.toString(commandSequence.charAt(currentCommandExec));
			currentCommandExec++;
			roverFacingDirectionUpdate(commandString);
		}
	}*/

	private char getRoverIncreasedY() {
		int currentY = roverStatus.charAt(ROVER_Y_INDEX) - '0';
		return (char)(currentY + 1 + '0');
	}
	
	private char getRoverDecreasedY() {
		int currentY = roverStatus.charAt(ROVER_Y_INDEX) - '0';
		return (char)(currentY - 1 + '0');
	}
	
	private char getRoverIncreasedX() {
		int currentX = roverStatus.charAt(ROVER_X_INDEX) - '0';
		return (char)(currentX + 1 + '0');
	}
	
	private char getRoverDecreasedX() {
		int currentX = roverStatus.charAt(ROVER_X_INDEX) - '0';
		return (char)(currentX - 1 + '0');
	}

}
