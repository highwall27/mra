package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;

import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testIfObstacleIsPlacedInANewPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		
		assertTrue( rover.planetContainsObstacleAt(2, 2) );
	}
	
	@Test
	public void testEmptyCommandExecutionShouldBeLandingStatus() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		
		assertEquals( "(0,0,N)" ,rover.executeCommand("") );
	}
	
	@Test
	public void testRoverTurningRightShouldReturnE() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		
		assertEquals( "(0,0,E)" ,rover.executeCommand("r") );
	}
	
	@Test
	public void testRoverTurningLeftShouldReturnW() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		
		assertEquals( "(0,0,W)" ,rover.executeCommand("l") );
	}
	
	@Test
	public void testMovingForwardShouldReturnYIncreased() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		
		assertEquals( "(0,1,N)" ,rover.executeCommand("f") );
	}
	
	@Test
	public void testMovingBackwardShouldReturnYDecreased() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		rover.executeCommand("f");
		assertEquals( "(0,0,N)" ,rover.executeCommand("b") );
	}
	
	@Test
	public void testMultipleCommandsGiven() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover rover = new MarsRover( 3, 3, planetObstacles);
		assertEquals( "(2,2,E)" ,rover.executeCommand("ffrff") );
	}
	
	

}
